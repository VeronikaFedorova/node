const express = require("express");
const morgan = require("morgan");
const fs = require("fs");
const app = express();
const port = 8080;
const arg = process.argv;

app.use(morgan("combined"));
app.use(express.json());

app.get("/api/files", (req, res) => {
  fs.readdir("./api/files", (err, files) => {
    files.forEach((file) => {
      console.log(file);
    });

    if (!files) {
      res.status(400).json({
        message: "Client error",
      });
    } else if (!err) {
      res.status(200).json({
        message: "Success",
        files: `[${files}]`,
      });
    } else {
      res.status(500).json({
        message: "Server error",
      });
    }
  });
});

app.post("/api/files", (req, res) => {
  let name = req.body.filename;
  let content = req.body.content;
  fs.writeFile(`./api/files/${name}`, content, function (err) {
    if (!content) {
      res.status(400).json({
        message: "Please specify 'content' parameter",
      });
    } else if (!err) {
      res.status(200).json({
        message: "File created successfully",
      });
    } else {
      res.status(500).json({
        message: "Server error",
      });
    }
  });
});

app.get(`/api/files/:filename`, (req, res) => {
  const file = req.params.filename;
  const regex = /[0-9a-z]+$/i;
  let ext = file.match(regex);
  const filename = `./api/files/${file}`;
  let birthtime;

  fs.stat(filename, (err, stats) => {
    birthtime = stats?.birthtime;
  })

  fs.readFile(`./api/files/${file}`, "utf8", (err, data) => {
    if (err) {
      res.status(400).json({
        "message" : `No file with ${file} filename found`
      });
    } else if (data) {
      res
      .status(200)
      .json({
        "message": "Success",
        "filename": `${file}`,
        "content": `${data}`,
        "extension": `${ext[0]}`,
        "uploadedDate": `${birthtime}`
      });
    } else {
        res
        .status(500)
        .json({
            "message": "Server error"
        })
    }
  });
});

app.get(`/api/files/delete/:filename`, (req, res) => {
    const file = req.params.filename;
    const filename = `./api/files/${file}`;
    fs.rm(filename, { recursive:true }, (err) => {
        if(err){
            res
            .status(400)
            .json({
                "message" : `No file with ${file} filename found`
            })
        } else if(file) {
            res
            .status(200)
            .json({
                "message" : "File successfully deleted"
            })
        } else {
            res
            .status(500)
            .json({
                "message" : "Server error"
            })
        }
    });
});

app.listen(port, () => {
  console.log(`Server is listening o http://localhost:${port}`);
});
